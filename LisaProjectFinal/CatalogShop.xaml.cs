﻿using LisaProject.DBLib;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour CatalogShop.xaml
    /// </summary>
    public partial class CatalogShop : Window
    {
        #region Fields

        private lisaprojectEntities _Entities;
        private ObservableCollection<catalog> _Catalogs;
        private ObservableCollection<shop> _Shops;
        private ObservableCollection<catalogshop> _CatalogShops;

        #endregion

        #region Constructors
        public CatalogShop()
        {
            _Entities = new lisaprojectEntities();
            InitializeComponent();
            RefreshListBoxCatalogs();
            RefreshListBoxShops();
            RefreshListBoxCatalogShop();
        }
        #endregion

        #region Methods
        // Raffraichissement de la liste des magasins
        private void RefreshListBoxShops()
        {
            _Shops = new ObservableCollection<shop>(_Entities.shops);
            _DataGridShops.ItemsSource = _Shops;
            _ComboBoxShop.ItemsSource = _Entities.shops.ToList();
        }

        // Raffraichissement de la liste des catalogues
        private void RefreshListBoxCatalogs()
        {
            _Catalogs = new ObservableCollection<catalog>(_Entities.catalogs);
            _DataGridCatalogs.ItemsSource = _Catalogs;
            _ComboBoxCatalog.ItemsSource = _Entities.catalogs.ToList();
        }
        
        private void RefreshListBoxCatalogShop()
        {
            _CatalogShops = new ObservableCollection<catalogshop>(_Entities.catalogshops);
            _DataGridCatalogShop.ItemsSource = _CatalogShops;
        }


        private void _Button_Click(object sender, RoutedEventArgs e)
        {
            // Instanciation d'une nouvelle relation
            catalogshop catalogShop= new catalogshop();
            catalogShop.catalog = (catalog)_ComboBoxCatalog.SelectedItem;
            catalogShop.shop = (shop)_ComboBoxShop.SelectedItem;
            catalogShop.DisplayStartDate = _DataPickerStartDate.SelectedDate;
            catalogShop.DisplayEndDate = _DataPickerEndDate.SelectedDate;

            // Sauvegarde des nouvelles données 
            _Entities.catalogshops.Add(catalogShop);
            _Entities.SaveChanges();
            // Raffraichissement de la liste
            RefreshListBoxCatalogShop();
            MessageBox.Show("Nouvel ajout d'un catalogue à un magasin", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);

            _ComboBoxCatalog.SelectedIndex = -1;
            _ComboBoxShop.SelectedIndex = -1;
            
        }
        
        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridCatalogShop.SelectedItems.Count == 1)
            {
                Relation window = new Relation((catalogshop)_DataGridCatalogShop.SelectedItem, new ObservableCollection<catalog>(_Entities.catalogs), new ObservableCollection<shop>(_Entities.shops));
                window.DataContext = _DataGridCatalogShop.SelectedItems;

                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Affectation catalogue/magasin modifiée", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);

                }
                RefreshListBoxCatalogShop();


            }
        }

        // Suppression de la relation d'un catalogue à un magasin
        private void _ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridCatalogShop.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer l'affectation choisie ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (catalogshop catalogShopToDelete in _DataGridCatalogShop.SelectedItems.OfType<catalogshop>().ToList())
                    {

                        //Supprimer l'enseigne dans le contexte entity framework
                        _Entities.catalogshops.Remove(catalogShopToDelete);
                        //La collection observable (et donc le DataGrid)
                        _CatalogShops.Remove(catalogShopToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Affectation supprimée", "Confirmation de suppression", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }
            }
        }
        #endregion
    }
}
