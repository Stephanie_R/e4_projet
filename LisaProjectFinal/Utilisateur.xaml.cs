﻿using LisaProject.DBLib;
using LisaProjectFinal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Utilisateur.xaml
    /// </summary>
    public partial class Utilisateur : Window
    {
        #region Fields
        private backofficeuser _User;
        #endregion

        #region Constructors
        public Utilisateur(backofficeuser user)
        {
            InitializeComponent();
            _User = user;
        }
        #endregion

        #region Methods
        private void _ButtonUpdateUser_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            //Modification d'un utilisateur

            _User.Prenom = _TextBoxPrenom.Text;
            _User.Nom = _TextBoxNom.Text;
            _User.Login = _TextBoxLogin.Text;
            _User.Password = Fonctions.Hash(_PasswordBox.Password);

            if (_User != null)
            {
                checkval = true;
            }else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
        #endregion
    }
}
