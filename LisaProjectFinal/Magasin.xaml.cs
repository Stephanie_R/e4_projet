﻿using LisaProject.DBLib;
using System.Linq;
using System.Windows;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Magasin.xaml
    /// </summary>
    public partial class Magasin : Window
    {
        private lisaprojectEntities _Entities;
        private shop _Mag;
        public Magasin(shop mag)
        {
            _Mag = mag;
            //Il faut toujours utiliser le meme datacontexte sinon les instances ne seront pas les mêmes
            //Il ne faut pas faire de new sur le _Entities
            _Entities = new lisaprojectEntities();
            InitializeComponent();
            
            _ComboBoxBigChain.ItemsSource = _Entities.bigchains.ToList();
            _ComboBoxBigChain.SelectedValuePath = "Identifier";
            

            _ComboBoxBigChain.SelectedValue = mag.bigchain.Identifier;


        }

        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            //Modification d'un magasin
            //C4est là) ::::!!!
            //Tu créés une nouvelle instance donc ça peut pas marcher
            
            _Mag.Code = _TextBoxCode.Text;
            _Mag.Name = _TextBoxNameShop.Text;
            _Mag.IdBigChain = (long)_ComboBoxBigChain.SelectedValue;
            
            if (_Mag != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
