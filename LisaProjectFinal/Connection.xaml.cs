﻿using System;
using LisaProject.DBLib;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using System.Security.Cryptography;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Connection.xaml
    /// </summary>
    public partial class Connection : Window
    {
        public Connection()
        {
            InitializeComponent();

        }


        private void _ButtonValid_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Hashage du mot de passe
            String password = Fonctions.Hash(PasswordBox.Password);
            
            // Vérification du login et du password rentrés avec la BDD
            backofficeuser user = db.backofficeusers.FirstOrDefault(x => x.Login == TextBoxLogin.Text && x.Password == password);
            
            // Si les identifiants n'existent pas
            if (user == null)
            {
                MessageBox.Show("Impossible de se connecter");                
            }
            // Si les identifiants existent
            else
            {
                //MessageBox.Show("Ca marche");
                this.DialogResult = true;
            }

           


        //operation o = new operation();
        //o.Code = "test";
        //o.StartDate = DateTime.Now;
        //o.EndDate = DateTime.Now.AddDays(100);
        //o.Title = "Opération 1";

        //db.operations.Add(o);
        //db.SaveChanges();
    }
        //private void passwordBox_Txt_KeyDown(object sender, KeyPressEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        PasswordBox.KeyDown();
        //        e.Handled = true;
        //    }
        //}
    }
}
