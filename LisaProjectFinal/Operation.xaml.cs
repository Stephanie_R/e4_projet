﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Operation.xaml
    /// </summary>
    public partial class Operation : Window
    {
        private lisaprojectEntities _Entities;
        private operation _Operation;

        public Operation(operation operation)
        {
            _Entities = new lisaprojectEntities();
            _Operation = operation;
            InitializeComponent();
        }

        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            _Operation.Code = _TextBoxCode.Text;
            _Operation.Title = _TextBoxTitle.Text;
            _Operation.StartDate = _DatePickerStartDate.SelectedDate;
            _Operation.EndDate = _DatePickerEndDate.SelectedDate;

            if (_Operation != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
