﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour CatalogOperation.xaml
    /// </summary>
    public partial class CatalogOperation : Window
    {
        #region Fields

        private lisaprojectEntities _Entities;
        private ObservableCollection<catalog> _Catalogs;
        private ObservableCollection<operation> _Operations;

        #endregion

        #region Constructors
        public CatalogOperation()
        {
            _Entities = new lisaprojectEntities();
            InitializeComponent();
            RefreshListBoxCatalogs();
            RefreshListBoxOperations();
        }
        #endregion

        #region Methods
        #region Operation commerciale

        // Raffraîchissement de la liste des opérations
        private void RefreshListBoxOperations()
        {
            _Operations = new ObservableCollection<operation>(_Entities.operations);
            _DataGridOperation.ItemsSource = _Operations;
        }

        // Ajout d'une opération commerciale
        private void _ButtonInsertOpe_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation d'une nouvelle opération
            operation operation = new operation();
            operation.Code = _TextBoxCodeOpe.Text;
            operation.Title = _TextBoxTitle.Text;
            operation.StartDate = _DataPickerStartDate.SelectedDate;
            operation.EndDate = _DataPickerEndDate.SelectedDate;

            // Sauvegarde des nouvelles données 
            _Entities.operations.Add(operation);
            db.operations.Add(operation);
            _Entities.SaveChanges();
            RefreshListBoxOperations();
            MessageBox.Show("Nouvelle opération ajoutée", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);

            _TextBoxCodeOpe.Clear();
            _TextBoxTitle.Clear();
        }

        // Suppression d'une opération commerciale
        private void _ButtonDeleteOpe_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridOperation.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show(
                    "Voulez-vous vraiment supprimer l'opération choisie ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (operation operationToDelete in _DataGridOperation
                                .SelectedItems.OfType<operation>().ToList())
                    {
                        //Supprimer l'opération dans le contexte entity framework
                        _Entities.operations.Remove(operationToDelete);
                        //La collection observable (et donc le DataGrid)
                        _Operations.Remove(operationToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Opération supprimée", "Confirmation de suppression"
                            , MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        // Modification d'une opération commerciale
        private void _ButtonUpdateOpe_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridOperation.SelectedItems.Count == 1)
            {
                Operation window = new Operation((operation)_DataGridOperation.SelectedItem);
                window.DataContext = _DataGridOperation.SelectedItems;

                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Opération commerciale modifiée", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);
                }
                RefreshListBoxOperations();
            }
        }

        #region Catalogue

        // Rafraîchissement de la liste des catalogues
        private void RefreshListBoxCatalogs()
        {
            _Catalogs = new ObservableCollection<catalog>(_Entities.catalogs);
            _DataGridCatalogs.ItemsSource = _Catalogs;
            _ComboBoxAgence.ItemsSource = _Entities.agences.ToList();
            _ComboBoxOperation.ItemsSource = _Entities.operations.ToList();
        }

        // Ajout d'un catalogue
        private void _ButtonInsertCatalog_Click(object sender, RoutedEventArgs e)
        {
            // Instanciation d'un nouveau catalogue
            catalog catalog = new catalog();
            catalog.Type = _TextBoxType.Text;
            catalog.Label = _TextBoxName.Text;
            if (!String.IsNullOrWhiteSpace(_TextBoxWidth.Text))
            {
                catalog.CatalogWidth = int.Parse(_TextBoxWidth.Text.Replace(".", ","));
            }
            else
            {
                catalog.CatalogWidth = null;
            }
            
            if (!String.IsNullOrWhiteSpace(_TextBoxHeight.Text))
            {
                catalog.CatalogHeight = int.Parse(_TextBoxHeight.Text.Replace(".", ","));
            }
            else
            {
                catalog.CatalogHeight = null;
            }
            
            catalog.agence = (agence)_ComboBoxAgence.SelectedItem;
            catalog.operation = (operation)_ComboBoxOperation.SelectedItem;

            // Sauvegarde des nouvelles données 
            _Entities.catalogs.Add(catalog);
            _Entities.SaveChanges();
            RefreshListBoxCatalogs();
            MessageBox.Show("Nouveau catalogue ajouté", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);

            // 
            _TextBoxType.Clear();
            _TextBoxName.Clear();
            _TextBoxWidth.Clear();
            _TextBoxHeight.Clear();
            _ComboBoxAgence.SelectedIndex = -1;
            _ComboBoxOperation.SelectedIndex = -1;
            
        }

        // Suppression d'un catalogue
        private void _ButtonDeleteCatalog_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridCatalogs.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer le catalogue choisi ?"
                    , "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (catalog catalogToDelete in _DataGridCatalogs.SelectedItems.OfType<catalog>().ToList())
                    {
                        //Supprimer l'utilisateur dans le contexte entity framework
                        _Entities.catalogs.Remove(catalogToDelete);
                        //La collection observable (et donc le DataGrid)
                        _Catalogs.Remove(catalogToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Catalogue supprimé", "Confirmation de suppression"
                            , MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        // Modification d'un catalogue
        private void _ButtonUpdateCatalog_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridCatalogs.SelectedItems.Count == 1)
            {
                Catalogue window = new Catalogue((catalog)_DataGridCatalogs.SelectedItem);
                window.DataContext = _DataGridCatalogs.SelectedItems;

                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Catalogue modifié", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);
                }
                RefreshListBoxCatalogs();
            }
        }
        #endregion
        #endregion

        // Lors de la sélection du catalogue, affichage de l'agence rattachée et de 
        // 'opération commerciale à ce catalogue dans un autre datagrid
        private void _DataGridCatalogs_SelectedCellsChanged(object sender, System.Windows.Controls.SelectedCellsChangedEventArgs e)
        {
            if (_DataGridCatalogs.SelectedItem != null)
            {
                List<operation> list = new List<operation>();
                List<agence> list2 = new List<agence>();
                catalog catalog = (catalog)_DataGridCatalogs.SelectedItem;
                list.Add(catalog.operation);
                _DataGridOpe.ItemsSource = list;
                list2.Add(catalog.agence);
                _DataGridAgence.ItemsSource = list2;
            }
            
        }
        #endregion

        private void _TextBoxWidth_TextChanged(object sender, TextChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in _TextBoxWidth.Text)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            _TextBoxWidth.Text = sb.ToString();
        }

        private void _TextBoxHeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in _TextBoxHeight.Text)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            _TextBoxHeight.Text = sb.ToString();
        }
    }
}
