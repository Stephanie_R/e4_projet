﻿using LisaProject.DBLib;
using System.Collections.ObjectModel;
using System.Windows;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Relation.xaml
    /// </summary>
    public partial class Relation : Window
    {
        
        private catalogshop _CatShop;
        
        public Relation(catalogshop catShop, ObservableCollection<catalog> catalogs, ObservableCollection<shop> shops)
        {
            _CatShop = catShop;
            
            InitializeComponent();

            // Initialisation de la liste des catalogues disponibles
            _ComboBoxCatalog.ItemsSource = catalogs;
            _ComboBoxCatalog.SelectedValuePath = "Identifier";
            _ComboBoxCatalog.SelectedValue = catShop.catalog.Identifier;

            // Initialisation de la liste des magasins disponibles
            _ComboBoxShop.ItemsSource = shops;
            _ComboBoxShop.SelectedValuePath = "Identifier";
            _ComboBoxShop.SelectedValue = catShop.shop.Identifier;

            //_DataPickerStartDate.SelectedDate
        }

        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            _CatShop.catalog = (catalog)_ComboBoxCatalog.SelectedItem;
            _CatShop.shop = (shop)_ComboBoxShop.SelectedItem;
            _CatShop.DisplayStartDate = _DataPickerStartDate.SelectedDate;
            _CatShop.DisplayEndDate = _DataPickerEndDate.SelectedDate;

            if (_CatShop != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
