﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour GestionCatalOpePagesProduits.xaml
    /// </summary>
    public partial class GestionCatalOpePagesProduits : Window
    {
        public GestionCatalOpePagesProduits()
        {
            InitializeComponent();
        }

        private void _ButtonCatalogOperation_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation de la nouvelle fenêtre
            CatalogOperation catalOpe = new CatalogOperation();
            // Ouverture de la fenêtre
            catalOpe.ShowDialog();
        }

        private void _ButtonPageProduit_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation de la nouvelle fenêtre
            PageProduit pageProduit = new PageProduit();
            // Ouverture de la fenêtre
            pageProduit.ShowDialog();
        }

        private void _ButtonCatalogShop_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation de la nouvelle fenêtre
            CatalogShop catalogShop = new CatalogShop();
            // Ouverture de la fenêtre
            catalogShop.ShowDialog();
        }
    }
}
