﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour PageProduit.xaml
    /// </summary>
    public partial class PageProduit : Window
    {
        #region Fields

        private lisaprojectEntities _Entities;
        private ObservableCollection<catalog> _Catalogs;
        private ObservableCollection<page> _Pages;
        private ObservableCollection<product> _Products;

        #endregion

        #region Constructors
        public PageProduit()
        {
            _Entities = new lisaprojectEntities();
            InitializeComponent();
            RefreshListBoxProducts();
            RefreshListBoxCatalogs();
        }

        private void RefreshListBoxCatalogs()
        {
            _Catalogs = new ObservableCollection<catalog>(_Entities.catalogs);
            _DataGridCatalog2.ItemsSource = _Catalogs;
            _ComboBoxCatalogChoice.ItemsSource = _Entities.catalogs.ToList();
        }


        // Rafraîchissement de la liste des produits
        private void RefreshListBoxProducts()
        {
            _Products = new ObservableCollection<product>(_Entities.products);
            _DataGridProducts.ItemsSource = _Products;
            _ComboBoxCatalog.ItemsSource = _Entities.catalogs.ToList();
            _ComboBoxPage.ItemsSource = _Entities.pages.ToList();
        }

        // Modification d'un produit
        private void _ButtonUpdateProduct_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridProducts.SelectedItems.Count == 1)
            {
                Produit window = new Produit((product)_DataGridProducts.SelectedItem, new ObservableCollection<catalog>(_Entities.catalogs));
                window.DataContext = _DataGridProducts.SelectedItems;

                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Produit modifié", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);
                }
                RefreshListBoxProducts();
            }
        }

        // Suppression d'un produit
        private void _ButtonDeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridProducts.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer le produit choisi ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (product productToDelete in _DataGridProducts.SelectedItems.OfType<product>().ToList())
                    {
                        //Supprimer le produit dans le contexte entity framework
                        _Entities.products.Remove(productToDelete);
                        //La collection observable (et donc le DataGrid)
                        _Products.Remove(productToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Produit supprimé", "Confirmation de suppression"
                            , MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        

        // Ajout d'un nouveau produit
        private void _ButtonInsertProduct_Click(object sender, RoutedEventArgs e)
        {
            // Instanciation d'un nouveau produit
            product product = new product();
            product.Code = _TextBoxCode.Text;
            product.Label = _TextBoxLabel.Text;
            product.Description = _TextBoxDescription.Text;
            if (!String.IsNullOrWhiteSpace(_TextBoxPrice.Text))
            {
                product.Price = double.Parse(_TextBoxPrice.Text.Replace(".", ","));
            }
            else
            {
                product.Price = null;
            }
            if (!String.IsNullOrWhiteSpace(_TextBoxReduction.Text))
            {
                product.Reduction_euro = double.Parse(_TextBoxReduction.Text.Replace(".", ","));
            }
            else
            {
                product.Reduction_euro = null;
            }
            product.Image = _TextBoxImage.Text;
            product.Mention = _TextBoxMention.Text;
            product.Packaging = _TextBoxPackaging.Text;
            product.Origin = _TextBoxOrigin.Text;
            if (!String.IsNullOrWhiteSpace(_TextBoxAdvantage.Text))
            {
                product.Advantage_percent = double.Parse(_TextBoxAdvantage.Text.Replace(".", ","));
            }
            else
            {
                product.Advantage_percent = null;
            }
            product.Category = _TextBoxCategory.Text;
            product.page = (page)_ComboBoxPage.SelectedItem;
            product.page.catalog = (catalog)_ComboBoxCatalog.SelectedItem;


            // Sauvegarde des nouvelles données 
            _Entities.products.Add(product);
            _Entities.SaveChanges();
            RefreshListBoxProducts();
            MessageBox.Show("Nouveau produit ajouté", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);
            
            _TextBoxAdvantage.Clear();
            _TextBoxCategory.Clear();
            _TextBoxCode.Clear();
            _TextBoxDescription.Clear();
            _TextBoxImage.Clear();
            _TextBoxLabel.Clear();
            _TextBoxMention.Clear();
            _TextBoxOrigin.Clear();
            _TextBoxPackaging.Clear();
            _TextBoxPrice.Clear();
            _TextBoxReduction.Clear();
            _ComboBoxCatalog.SelectedIndex = -1;
            _ComboBoxPage.SelectedIndex = -1;

        }
        

        // Ajout d'une nouvelle page
        private void _ButtonInsertPage_Click(object sender, RoutedEventArgs e)
        {
            // Rechercher si la page existe déjà pour le catalogue
            //bool haspagenum = _Entities.pages.Where(p => p.Number == ((page)_DataGridPages.SelectedItem).Number).Any();
            //if (haspagenum == true)
            //{
            //    MessageBox.Show("Page déjà existante");
            //}
            
                page page = new page();
                page.catalog = (catalog)_ComboBoxCatalogChoice.SelectedItem;
                page.Number = int.Parse(_TextBoxPage.Text);

                // Sauvegarde des nouvelles données 
                _Entities.pages.Add(page);
                _Entities.SaveChanges();
                //RefreshListBoxPages();
                MessageBox.Show("Nouvelle page ajoutée", "Confirmation d'ajout"
                    , MessageBoxButton.OK, MessageBoxImage.Information);

                _ComboBoxCatalogChoice.SelectedIndex = -1;
                _TextBoxPage.Clear();
            
            
        }

        // Modification d'une page
        private void _ButtonUpdatePage_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridPages.SelectedItems.Count == 1)
            {
                Page window = new Page((page)_DataGridPages.SelectedItem, new ObservableCollection<catalog>(_Entities.catalogs));
                window.DataContext = _DataGridPages.SelectedItems;

                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Page modifiée", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);
                }
                //RefreshListBoxPages();
            }
        }

        // Suppression d'une page
        private void _ButtonDeletePage_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridPages.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer la page choisie ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (page pageToDelete in _DataGridPages.SelectedItems.OfType<page>().ToList())
                    {
                        //Supprimer le produit dans le contexte entity framework
                        _Entities.pages.Remove(pageToDelete);
                        //La collection observable (et donc le DataGrid)
                        _Pages.Remove(pageToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Page supprimée", "Confirmation de suppression"
                            , MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        // Récupérer que des nombres pour les TextBoxes suivantes
        private void _TextBoxPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in _TextBoxPrice.Text)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            _TextBoxPrice.Text = sb.ToString();
        }

        private void _TextBoxReduction_TextChanged(object sender, TextChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in _TextBoxPrice.Text)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            _TextBoxPrice.Text = sb.ToString();
        }

        private void _TextBoxAdvantage_TextChanged(object sender, TextChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in _TextBoxAdvantage.Text)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            _TextBoxAdvantage.Text = sb.ToString();
        }
        
        // Permet d'avoir les pages créées pour un catalogue sélectionné 
        private void _DataGridCatalog2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_DataGridCatalog2.SelectedItem != null)
            {
                catalog selectedCatalog = (catalog)(_DataGridCatalog2.SelectedItem);
                IEnumerable<page> pages = _Entities.pages.Where(x => x.IdCatalog == selectedCatalog.Identifier);
                _Pages = new ObservableCollection<page>(pages);
                _DataGridPages.ItemsSource = _Pages;
            }
            

        }

        // Permet d'avoir le catalogue et la page du produit
        private void _DataGridProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_DataGridProducts.SelectedItem != null)
            {
                product product = (product)_DataGridProducts.SelectedItem;
                _DataGridPage.ItemsSource = new List<page>() { product.page };
                _DataGridCatalog.ItemsSource = new List<catalog>() { product.page.catalog };
            }
        }

        // Récupère les pages existantes d'un catalogue
        private void _ComboBoxCatalog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this._ComboBoxPage.ItemsSource = ((catalog)_ComboBoxCatalog.SelectedItem).pages;
        }
    }



    #endregion
}
