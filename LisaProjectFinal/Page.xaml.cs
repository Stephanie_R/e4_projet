﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Page.xaml
    /// </summary>
    public partial class Page : Window
    {
        private page _Page;

        public Page(page page, ObservableCollection<catalog> catalogs)
        {
            _Page = page;
            InitializeComponent();

            // Initialisation de la liste des catalogues disponibles
            _ComboBoxCatalog.ItemsSource = catalogs.ToList();
            _ComboBoxCatalog.SelectedValuePath = "Identifier";
            _ComboBoxCatalog.SelectedValue = page.catalog.Identifier;
        }

        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            _Page.catalog = (catalog)_ComboBoxCatalog.SelectedItem;
            _Page.Number = int.Parse(_TextBoxPage.Text);

            if (_Page != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
