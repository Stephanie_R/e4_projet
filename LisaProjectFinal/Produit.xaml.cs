﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Produit.xaml
    /// </summary>
    public partial class Produit : Window
    {
      
        private product _Prod;

        public Produit(product prod, ObservableCollection<catalog> catalogs)
        {
            _Prod = prod;
      
            InitializeComponent();

            // Initialisation de la liste des catalogues disponibles
            _ComboBoxCatalog.ItemsSource = catalogs;
            _ComboBoxCatalog.SelectedValuePath = "Identifier";
            _ComboBoxCatalog.SelectedValue = prod.page.catalog.Identifier;

            // Initialisation de la liste des pages disponibles
            _ComboBoxPage.ItemsSource = prod.page.catalog.pages;
            _ComboBoxPage.SelectedValuePath = "Identifier";
            _ComboBoxPage.SelectedValue = prod.page.catalog.pages;
        }

        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            _Prod.Code = _TextBoxCode.Text;
            _Prod.Label = _TextBoxLabel.Text;
            _Prod.Description = _TextBoxDescription.Text;
            if (!String.IsNullOrWhiteSpace(_TextBoxPrice.Text))
            {
                _Prod.Price = double.Parse(_TextBoxPrice.Text.Replace(".", ","));
            }
            else
            {
                _Prod.Price = null;
            }
            if (!String.IsNullOrWhiteSpace(_TextBoxReduction.Text)){
                _Prod.Reduction_euro = double.Parse(_TextBoxReduction.Text.Replace(".", ","));
            }
            else
            {
                _Prod.Reduction_euro = null;
            }
            _Prod.Image = _TextBoxImage.Text;
            _Prod.Mention = _TextBoxMention.Text;
            _Prod.Packaging = _TextBoxPackaging.Text;
            _Prod.Origin = _TextBoxOrigin.Text;
            if (!String.IsNullOrWhiteSpace(_TextBoxAdvantage.Text))
            {
                _Prod.Advantage_percent = double.Parse(_TextBoxAdvantage.Text.Replace(".", ","));
            }
            else
            {
                _Prod.Advantage_percent = null;
            }
           
            _Prod.Category = _TextBoxCategory.Text;
            _Prod.page = (page)_ComboBoxPage.SelectedItem;
            _Prod.page.catalog = (catalog)_ComboBoxCatalog.SelectedItem;

            if (_Prod != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }


        }

        private void _ComboBoxCatalog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this._ComboBoxPage.ItemsSource = ((catalog)_ComboBoxCatalog.SelectedItem).pages;
        }
    }
}
