﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Enseigne.xaml
    /// </summary>
    public partial class Enseigne : Window
    {
        private bigchain _BigChain;
        public Enseigne(bigchain bigchain)
        {
            _BigChain = bigchain;
            InitializeComponent();
        }

        private void _ButtonUpdateBigChain_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            //Modification d'une enseigne
            _BigChain.Label = _TextBoxLabel.Text;
            

            if (_BigChain != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
