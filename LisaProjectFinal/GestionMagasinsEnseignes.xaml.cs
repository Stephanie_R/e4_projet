﻿using LisaProject.DBLib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour GestionMagasinsEnseignes.xaml
    /// </summary>
    public partial class GestionMagasinsEnseignes : Window
    {
        #region Fields

        private lisaprojectEntities _Entities;
        private ObservableCollection<bigchain> _BigChains;
        private ObservableCollection<shop> _Shops;

        #endregion

        #region Constructors
        public GestionMagasinsEnseignes()
        {
            _Entities = new lisaprojectEntities();
            InitializeComponent();
            RefreshListBoxBigChains();
            RefreshListBoxShops();
        }
        #endregion

        #region Methods

        // Modification d'une enseigne
        private void _ButtonUpdateBC_Click(object sender, RoutedEventArgs e)
        {

            if (_DataGridBigChains.SelectedItems.Count == 1)
            {
                Enseigne window = new Enseigne((bigchain)_DataGridBigChains.SelectedItem);
                window.DataContext = _DataGridBigChains.SelectedItems;

                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Enseigne modifiée", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);

                }
                RefreshListBoxBigChains();


            }
        }

        // Suppression d'une enseigne
        private void _ButtonDeleteBC_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridBigChains.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer l'enseigne choisie ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (bigchain bigChainToDelete in _DataGridBigChains.SelectedItems.OfType<bigchain>().ToList())
                    {

                        //Supprimer l'enseigne dans le contexte entity framework
                        _Entities.bigchains.Remove(bigChainToDelete);
                        //La collection observable (et donc le DataGrid)
                        _BigChains.Remove(bigChainToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Enseigne supprimée", "Confirmation de suppression", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }
            }
        }

        // Ajout d'une enseigne
        private void _ButtonInsertBigChain_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation d'une nouvelle enseigne
            bigchain chain = new bigchain();
            chain.Label = _TextBoxBigChain.Text;

            // Sauvegarde des nouvelles données 
            _Entities.bigchains.Add(chain);
            db.bigchains.Add(chain);
            _Entities.SaveChanges();
            RefreshListBoxBigChains();
            MessageBox.Show("Nouvelle enseigne ajoutée", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);

            _TextBoxBigChain.Clear();
        }

        // Raffraichissement de la liste des enseignes
        private void RefreshListBoxBigChains()
        {
            _BigChains = new ObservableCollection<bigchain>(_Entities.bigchains);
            _DataGridBigChains.ItemsSource = _BigChains;
            
        }

        // Raffraichissement de la liste des magasins
        private void RefreshListBoxShops()
        {
            _Shops = new ObservableCollection<shop>(_Entities.shops);
            _DataGridShops.ItemsSource = _Shops;
            _ComboBoxBigChain.ItemsSource = _Entities.bigchains.ToList();
        }

        // Mise à jour d'un magasin
        private void _ButtonUpdateShop_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridShops.SelectedItems.Count == 1)
            {
                Magasin window = new Magasin((shop)_DataGridShops.SelectedItem);
                window.DataContext = _DataGridShops.SelectedItems;
               
                window.ShowDialog();

                bool? result = window.DialogResult;

                if (result == true)
                {
                    _Entities.SaveChanges();

                    MessageBox.Show("Magasin modifié", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);
                }
                RefreshListBoxShops();
            }
        }

        // Suppression d'un magasin
        private void _ButtonDeleteShop_Click(object sender, RoutedEventArgs e)
        {
            if (_DataGridShops.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer le magasin choisi ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (shop shopToDelete in _DataGridShops.SelectedItems.OfType<shop>().ToList())
                    {

                        //Supprimer le magasin dans le contexte entity framework
                        _Entities.shops.Remove(shopToDelete);
                        //La collection observable (et donc le DataGrid)
                        _Shops.Remove(shopToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Magasin supprimé", "Confirmation de suppression", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }
            }
        }

        // Ajout d'un nouveau magasin
        private void _ButtonInsertShop_Click(object sender, RoutedEventArgs e)
        {
            
            // Instanciation d'un nouveau magasin
            shop shop = new shop();
            shop.Code = _TextBoxCode.Text;
            shop.Name = _TextBoxNameShop.Text;
            shop.bigchain = (bigchain)_ComboBoxBigChain.SelectedItem;


            // Sauvegarde des nouvelles données 
            _Entities.shops.Add(shop);
            _Entities.SaveChanges();
            // Raffraichissement de la liste
            RefreshListBoxShops();
            MessageBox.Show("Nouveau magasin ajouté", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);
             
            _TextBoxCode.Clear();
            _TextBoxNameShop.Clear();
            _ComboBoxBigChain.SelectedIndex = -1;
        }

        // Lors de la sélection du magasin, affichage de l'enseigne rattachée
        // à ce magasin dans un autre datagrid
        private void _DataGridShops_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (_DataGridShops.SelectedItem != null)
            {
                List<bigchain> list = new List<bigchain>();
                shop shop = (shop)_DataGridShops.SelectedItem;
                list.Add(shop.bigchain);
                _DataGridBigChain.ItemsSource = list;
            }
        }

        #endregion
    }
}
