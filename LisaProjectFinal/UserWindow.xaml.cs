﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LisaProject.DBLib;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        #region Fields

        private lisaprojectEntities _Entities;
        private ObservableCollection<backofficeuser> _Users;         
        private List<agence> _Agences;
        #endregion

        #region Constructors

        public UserWindow()
        {
            _Entities = new lisaprojectEntities();
            InitializeComponent();
            RefreshListBoxUsers();
            RefreshListBoxAgences();
        }

        #endregion

        #region Methods
        #region Users

        //Ajout d'un utilisateur
        private void _ButtonInsertUser_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            //lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation d'un nouvel utilisateur
            backofficeuser user = new backofficeuser();
            user.Prenom = _TextBoxPrenom.Text;
            user.Nom = _TextBoxNom.Text;
            user.Login = _TextBoxLogin.Text;
            user.Password = Fonctions.Hash(_PasswordBox.Password);

            // Sauvegarde des nouvelles données 
            _Entities.backofficeusers.Add(user);
            //db.backofficeusers.Add(user);
            _Entities.SaveChanges();
            RefreshListBoxUsers();
            MessageBox.Show("Nouvel utilisateur ajouté", "Confirmation d'ajout"
                , MessageBoxButton.OK, MessageBoxImage.Information);

            _TextBoxPrenom.Clear();
            _TextBoxNom.Clear();
            _TextBoxLogin.Clear();
            _PasswordBox.Clear();

        }

        // Modification d'un utilisateur
        private void _ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            //lisaprojectEntities db = new lisaprojectEntities();
            if (_DataGridUsers.SelectedItems.Count == 1)
            {
                Utilisateur window = new Utilisateur((backofficeuser)_DataGridUsers.SelectedItem);
                window.DataContext = _DataGridUsers.SelectedItems;

                window.ShowDialog();
                
                bool? result = window.DialogResult;
                
                if (result == true)
                {
                    _Entities.SaveChanges();
                    
                    MessageBox.Show("Utilisateur modifié", "Confirmation de modification"
                        , MessageBoxButton.OK, MessageBoxImage.Information);
                    
                }
                RefreshListBoxUsers();


            }
        }
        //Suppression d'un utilisateur
        private void _ButtonDelete_Click(object sender, RoutedEventArgs e)
        {            
            if (_DataGridUsers.SelectedItems.Count > 0)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Voulez-vous vraiment supprimer l'utilisateur choisi ?", "Supprimer"
                    , MessageBoxButton.YesNo
                    , MessageBoxImage.Question))
                {
                    foreach (backofficeuser userToDelete in _DataGridUsers.SelectedItems.OfType<backofficeuser>().ToList())
                    {

                        //Supprimer l'utilisateur dans le contexte entity framework
                        _Entities.backofficeusers.Remove(userToDelete);
                        //La collection observable (et donc le DataGrid)
                        _Users.Remove(userToDelete);
                        // Supprimer dans la BDD
                        _Entities.SaveChanges();

                        MessageBox.Show("Utilisateur supprimé", "Confirmation de suppression", MessageBoxButton.OK, MessageBoxImage.Information);
                        
                    }
                }
            }
            
        }
        // Rafraîchissement de la liste des utilisateurs
        private void RefreshListBoxUsers()
        {
            _Users = new ObservableCollection<backofficeuser>(_Entities.backofficeusers);
            _DataGridUsers.ItemsSource = _Users;
            
        }

        #endregion

        #region Agences

        private void RefreshListBoxAgences()
        {
            _Agences = _Entities.agences.ToList();
            _DataGridAgences.ItemsSource = _Agences;
        }

        #endregion

        #endregion



    }
}
