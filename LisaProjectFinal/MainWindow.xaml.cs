﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //Lancement de la fenêtre de connexion
            Connection window = new Connection();
            window.ShowDialog();
            // Si la connexion n'est pas possible, fermer le back-office
            if (window.DialogResult == false)
                this.Close();
        }
        
        // Gestion de utilisateurs (et visualisation des agences)
        private void _ButtonUsers_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation de la nouvelle fenêtre
            UserWindow userWindow = new UserWindow();
            // Ouverture de la fenêtre
            userWindow.ShowDialog();
        }

        // Gestion des catalogues
        private void _ButtonCatalogs_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation de la nouvelle fenêtre
            GestionCatalOpePagesProduits window = new GestionCatalOpePagesProduits();
            // Ouverture de la fenêtre
            window.ShowDialog();
        }

        // Gestion des magasins
        private void _ButtonShops_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaprojectEntities db = new lisaprojectEntities();
            // Instanciation de la nouvelle fenêtre
            //Shop window = new Shop();
            GestionMagasinsEnseignes window = new GestionMagasinsEnseignes();
            // Ouverture de la fenêtre
            window.ShowDialog();
        }

        // Déconnexion du back-office
        private void _ButtonDisconnect_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
