﻿using LisaProject.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LisaProjectFinal
{
    /// <summary>
    /// Logique d'interaction pour Catalogue.xaml
    /// </summary>
    public partial class Catalogue : Window
    {
        private lisaprojectEntities _Entities;
        private catalog _Catalog;

        public Catalogue(catalog cat)
        {
            _Entities = new lisaprojectEntities();
            _Catalog = cat;
            InitializeComponent();

            // Initialisation de la liste des opérations disponibles
            _ComboBoxOperation.ItemsSource = _Entities.operations.ToList();
            _ComboBoxOperation.SelectedValuePath = "Identifier";
            _ComboBoxOperation.SelectedValue = cat.operation.Identifier;

            // Initialisation de la liste des agences disponibles
            _ComboBoxAgence.ItemsSource = _Entities.agences.ToList();
            _ComboBoxAgence.SelectedValuePath = "Identifier";
            _ComboBoxAgence.SelectedValue = cat.agence.Identifier;
        }

        private void _ButtonUpdateCat_Click(object sender, RoutedEventArgs e)
        {
            bool checkval = false;

            _Catalog.Type = _TextBoxType.Text;
            _Catalog.Label = _TextBoxLabel.Text;
            if (!String.IsNullOrWhiteSpace(_TextBoxWidth.Text))
            {
                _Catalog.CatalogWidth = int.Parse(_TextBoxWidth.Text.Replace(".", ","));
            }
            else
            {
                _Catalog.CatalogWidth = null;
            }

            if (!String.IsNullOrWhiteSpace(_TextBoxHeight.Text))
            {
                _Catalog.CatalogHeight = int.Parse(_TextBoxHeight.Text.Replace(".", ","));
            }
            else
            {
                _Catalog.CatalogHeight = null;
            }
            _Catalog.IdOperation = (long)_ComboBoxOperation.SelectedValue;
            _Catalog.IdAgence = (long)_ComboBoxAgence.SelectedValue;

            if (_Catalog != null)
            {
                checkval = true;
            }
            else
            {
                checkval = false;
            }

            if (checkval)
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
