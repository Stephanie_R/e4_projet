﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lisa.DBLib;

namespace Back_Office_Lisa
{
    /// <summary>
    /// Logique d'interaction pour Connexion.xaml
    /// </summary>
    public partial class Connexion : Window
    {
        public Connexion()
        {
            InitializeComponent();
        }

        private void _ButtonValid_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaEntities db = new lisaEntities();
            // Vérification du login et du password rentrés avec la BDD
            backofficeuser user = db.backofficeusers.FirstOrDefault(x => x.Login == TextBoxLogin.Text && x.Password == PasswordBox.Password);

            // Si les identifiants n'existent pas
            if (user == null)
            {
                MessageBox.Show("Impossible de se connecter");
            }
            // Si les identifiants existent
            else
            {
                //MessageBox.Show("Ca marche");
                this.DialogResult = true;
            }
        }
    }
}
