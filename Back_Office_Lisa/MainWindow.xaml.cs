﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lisa.DBLib;

namespace Back_Office_Lisa
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //Lancement de la fenêtre de connexion
            Connexion window = new Connexion();
            window.ShowDialog();
            // Si la connexion n'est pas possible, fermer le back-office
            if (window.DialogResult == false)
                this.Close();
        }

        private void _ButtonUsers_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaEntities db = new lisaEntities();
            User window = new User();
            window.ShowDialog();
        }

        private void _ButtonCatalogs_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaEntities db = new lisaEntities();
        }

        private void _ButtonShops_Click(object sender, RoutedEventArgs e)
        {
            // Création de la connexion
            lisaEntities db = new lisaEntities();
        }

        private void _ButtonDisconnect_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
